import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyDetailsComponent } from './user-home/user-content/company-details/company-details.component';
import { UserContentComponent } from './user-home/user-content/user-content.component';
import { UserDetailsComponent } from './user-home/user-content/user-details/user-details.component';
import { UserHomeComponent } from './user-home/user-home.component';
const routes: Routes = [
  {
    path: '',
    component: UserHomeComponent,
    children: [
      {
        path: ':id',
        component: UserContentComponent,
        children: [
          {
            path: 'user-details/:id',
            component: UserDetailsComponent,
          },
          {
            path: 'company-details/:id',
            component: CompanyDetailsComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
