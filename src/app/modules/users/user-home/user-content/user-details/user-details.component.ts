import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Iuser } from 'src/app/common/type';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent implements OnInit {
  selectedId: number = 0;
  selectedUser!: Iuser;
  constructor(private route: ActivatedRoute, private http: DataService) {}
  ngOnInit(): void {
    //   this.selectedId = this.route.snapshot.params['id'];
    //   console.log(this.selectedId);
    this.route.params.subscribe((params: Params) => {
      this.selectedId = Number.parseInt(params['id']);
    });
    this.http.getAllUsers().subscribe({
      next: (response: any) => {
        for (let user of response) {
          if (user.id === this.selectedId) {
            this.selectedUser = user;
            console.log(this.selectedUser, 'yebeee');
          }
        }
      },
    });
  }
}
