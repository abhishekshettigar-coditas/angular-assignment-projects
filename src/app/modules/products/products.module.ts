import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsHomeComponent } from './products-home/products-home.component';
import { ItemsComponent } from './products-home/items/items.component';
import { OrdersComponent } from './products-home/orders/orders.component';


@NgModule({
  declarations: [
    ProductsHomeComponent,
    ItemsComponent,
    OrdersComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule
  ]
})
export class ProductsModule { }
